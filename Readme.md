# RichDivergence

### How do I get set up? ###

This repository contains submodules. Use the following command to check out the code:

```
git clone --recurse-submodules git@bitbucket.org:phaentu/richdivergence.git
```

# Updating
When updating the repository, the main repository and the submodules need to be updated seperatly.
To update the main code use:

```
git pull
```

And for the submodules:

```
git submodule update --recursive --remote
```