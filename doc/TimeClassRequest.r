n_I=100   # Number of discrete intervals
Tmax<-4  # Maximum time
pop<-3;

# ********************************************************************** #
# ****   Time discretization, and  splitting times **************** #

TimesAndSplittingTimes<-DiscreteTimeAndSplittingTimes(n_I,Tmax,pop);
Times<-TimesAndSplittingTimes[[1]];
T<-TimesAndSplittingTimes[[2]];
SpliTimeUpdate<-UpdateSplittingTimes(T,Times,Tmax)

#  ******Time discretization and initialization of the splitting times   ********** #

DiscreteTimeAndSplittingTimes<-function(n_I,Tmax,pop){
  Ci<-vector();
  for(i in 1:n_I){
    Ci[i]<-0.1*exp((i/n_I)*log(1+10*Tmax))-0.1; 
  }
  SplittingTimes<-sample(Ci, (pop-1), replace = FALSE, prob = NULL);
  listTimes<-list();
  listTimes[[1]]<- Ci;
  listTimes[[2]]<- sort(SplittingTimes);
  return(listTimes)
}

# Function to update the splitting times

UpdateSplittingTimes<-function(SplittingTimes1,Vect_Time_discrete,Tmax){ 
  SplitI<-sample((1:(pop-1)),1); # index of the splitting time to update
  u<-runif(1);
  index_discreteTime<-which(Vect_Time_discrete==SplittingTimes1[SplitI]);
  # just go up if the splitting time1 is 0
  if(SplittingTimes1[SplitI]==Vect_Time_discrete[1]){SplittingTimes1[SplitI]<-Vect_Time_discrete[2];}
  # just go down if the splitting time1 is Tmax
  if(SplittingTimes1[SplitI]==Vect_Time_discrete[length(Vect_Time_discrete)-1]){SplittingTimes1[SplitI]<-Vect_Time_discrete[index_discreteTime-2];}
  # To not exceed the limits
  if((SplitI<(length(SplittingTimes1)-1))&&(SplittingTimes1[SplitI]==SplittingTimes1[SplitI+1])){SplittingTimes1[SplitI]<-Vect_Time_discrete[index_discreteTime-1];}
  if((SplitI!=1)&&(SplitI<(length(SplittingTimes1)-1))&&(SplittingTimes1[SplitI]==SplittingTimes1[SplitI-1])){SplittingTimes1[SplitI]<-Vect_Time_discrete[index_discreteTime+1];}
  if((SplittingTimes1[SplitI]!=Vect_Time_discrete[1])&&(SplittingTimes1[SplitI]!=Vect_Time_discrete[length(Vect_Time_discrete)-1])){
    
    if(u<0.5) {SplittingTimes1[SplitI]<-Vect_Time_discrete[index_discreteTime-1];} else{SplittingTimes1[SplitI]<-Vect_Time_discrete[index_discreteTime+1];} 
    
    # [SplitI]
    return(SplittingTimes1);
  }
  
}

